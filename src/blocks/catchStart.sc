theme: /Blocks

    state:
        # Обработка /start для всех метаблоков
        q: * *start
        #event: noMatch
  
        if: $client["context-switch"] && $request.botId != $client["context-switch"].currentBotId
            script:
                $response.replies = $response.replies || [];
                $response.replies.push({
                    type: "context-return",
                    data: {}
                });
        else:
            if: Zenflow
                script:
                    Zenflow.start();
            else:
                go!: /