function returnByTransitionOrContextSwitch (customTagParams, state) {
    var $context = $jsapi.context();
    var $response = $context.response;
    var $request = $context.request;

    log("Custom tag params: " + JSON.stringify(customTagParams))
    log("Request data: " + JSON.stringify($request.data))

    if (customTagParams.withContextSwitch) {
        if (state) {
            $response.replies = $response.replies || [];
            $response.replies.push({
                type: "context-return",
                state: state,
                data: $request.data.original
            });
        } else {
            $response.replies = $response.replies || [];
            $response.replies.push({
                type: "context-return",
                state: "/",
                data: $request.data.original
            });
        }
    } else {
        if (state) {
            $reactions.transition(state)
        } else {
            $reactions.transition("/")
        }
    }
}